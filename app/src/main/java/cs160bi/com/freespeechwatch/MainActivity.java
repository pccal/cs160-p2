package cs160bi.com.freespeechwatch;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;


public class MainActivity extends Activity {

    final Context context = this;
    int drawingColor = Color.argb(255,255,0,0);
    int drawingSize = 10;
    boolean pen_active = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View sizeButton = findViewById(R.id.brush);
        View colorButton = findViewById(R.id.color);
        View penButton = findViewById(R.id.pen);
        View eraserButton = findViewById(R.id.eraser);
        View submitButton = findViewById(R.id.submit_btn);
        final DrawingView draw = (DrawingView) findViewById(R.id.drawing);

        colorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ColorDialog customDialog = new ColorDialog(context, drawingColor);
                customDialog.show();
            }
        });
        sizeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SizeDialog sizeDialog = new SizeDialog(context,20);
                sizeDialog.show();
            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        penButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPenStatus(true);
            }
        });
        eraserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPenStatus(false);
            }
        });

    }



    public void setDrawingColor(int color) {
        drawingColor = color;
        View colorBtn = (View) findViewById(R.id.color);
        DrawingView draw = (DrawingView) findViewById(R.id.drawing);
        colorBtn.setBackgroundColor(color);
        draw.setPaintColor(color);

    }

    public void setDrawingSize(int size) {
        drawingSize = size;
        DrawingView draw = (DrawingView) findViewById(R.id.drawing);
        draw.setSize(size);
    }

    public void setPenStatus(boolean active) {
        pen_active = active;

        View pen = findViewById(R.id.pen);
        View eraser = findViewById(R.id.eraser);
        DrawingView draw = (DrawingView) findViewById(R.id.drawing);

        int active_color = getResources().getColor(R.color.active);
        int inactive_color = getResources().getColor(R.color.inactive);
        if (pen_active)
        {
            pen.setBackgroundColor(active_color);
            eraser.setBackgroundColor(inactive_color);
            draw.enablePen();
        } else
        {
            eraser.setBackgroundColor(active_color);
            pen.setBackgroundColor(inactive_color);
            draw.enableEraser();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
