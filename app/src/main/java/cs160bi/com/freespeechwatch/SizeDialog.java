package cs160bi.com.freespeechwatch;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;

/**
 * Created by peterson_cheng on 10/12/14.
 */
public class SizeDialog extends AlertDialog {

    int outSize;
    ImageView radius;
    SeekBar sizeBar;

    SeekBar.OnSeekBarChangeListener sizeChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (!fromUser) {

            } else {
                radius.getLayoutParams().height = progress;
                radius.getLayoutParams().width = progress;
                radius.requestLayout();
                outSize = progress;

            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    public SizeDialog(Context context, int size) {
        super(context);
        outSize = size;
        if (context instanceof Activity) {
            setOwnerActivity((Activity) context);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_size);
        getViews();

        Button cancelBtn = (Button) findViewById(R.id.cancelBtn);
        Button setBtn = (Button) findViewById(R.id.setBtn);

        sizeBar.setOnSeekBarChangeListener(sizeChangeListener);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
        setBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getOwnerActivity()).setDrawingSize(outSize);
                dismiss();
            }
        });
    }

    private void getViews() {
        radius = (ImageView) findViewById(R.id.brush_img);
        sizeBar = (SeekBar) findViewById(R.id.sizeSlider);
    }

}
