package cs160bi.com.freespeechwatch;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.SeekBar;

/**
 * Created by peterson_cheng on 10/12/14.
 */
public class ColorDialog extends AlertDialog {

    int outColor;
    View colorDisplay;
    SeekBar redBar;
    SeekBar greenBar;
    SeekBar blueBar;
    SeekBar alphaBar;

    SeekBar.OnSeekBarChangeListener colorChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (!fromUser) {

            } else {
                setColor(Color.argb(alphaBar.getProgress(),
                                    redBar.getProgress(),
                                    greenBar.getProgress(),
                                    blueBar.getProgress()));
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    public ColorDialog(Context context, int color) {
        super(context);
        outColor = color;
        if (context instanceof Activity) {
            setOwnerActivity((Activity) context);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_color);
        getViews();

        redBar.setOnSeekBarChangeListener(colorChangeListener);
        greenBar.setOnSeekBarChangeListener(colorChangeListener);
        blueBar.setOnSeekBarChangeListener(colorChangeListener);
        alphaBar.setOnSeekBarChangeListener(colorChangeListener);

        setColor(outColor);

        Button cancelBtn = (Button) findViewById(R.id.cancelBtn);
        Button setBtn = (Button) findViewById(R.id.setBtn);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
        setBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getOwnerActivity()).setDrawingColor(outColor);
                dismiss();
            }
        });
    }

    public void setColor(int color) {
        redBar.setProgress(Color.red(color));
        greenBar.setProgress(Color.green(color));
        blueBar.setProgress(Color.blue(color));
        alphaBar.setProgress(Color.alpha(color));
        colorDisplay.setBackgroundColor(color);
        outColor = color;
    }

    private void getViews() {
        colorDisplay = findViewById(R.id.colorShow);
        redBar = (SeekBar) findViewById(R.id.redSlider);
        greenBar = (SeekBar) findViewById(R.id.greenSlider);
        blueBar = (SeekBar) findViewById(R.id.blueSlider);
        alphaBar = (SeekBar) findViewById(R.id.alphaSlider);
    }

}
